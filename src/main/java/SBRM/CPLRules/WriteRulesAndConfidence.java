package SBRM.CPLRules;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

public class WriteRulesAndConfidence {

	
	public static void WriteRulesToExcel(String filepath, CPLRule[] rules, String sheetName) {
    	int lowConfidenceRules =0;
		for (int i=0; i<rules.length;i++){
			rules[i].printRule();
    		if ((rules[i].getSupport()/ (rules[i].getSupport()+rules[i].getViolate()))<0.9){
				lowConfidenceRules++;
    		}
    	}
		double percentage= ((double) lowConfidenceRules/rules.length)*100;
		System.out.println("Total %age of Low Confidence Constraints is: "+percentage+ " %");
		System.out.println("Total Low Confidence Constraints are: "+lowConfidenceRules);
		
		//	CPLRule [] rules = new CPLRule [20];
		Sheet sheet = null;
		try {
			InputStream inp;
			Workbook wb;

			int sheetnumber = 0;
			File file = new File(filepath);
			
			if (file.exists()){
				inp = new FileInputStream(filepath);
				wb = WorkbookFactory.create(inp);
				wb.createSheet(sheetName);
				sheet = wb.getSheetAt(wb.getNumberOfSheets()-1);
			}
			else{
				wb= new XSSFWorkbook();
				wb.createSheet(sheetName);
				sheet = wb.getSheetAt(wb.getNumberOfSheets()-1);
			}

			Row row = sheet.createRow(0);
			Cell cell1 = row.createCell(0);
			cell1.setCellType(1);
			cell1.setCellValue("constraintExpression");

			cell1 = row.createCell(1);
			cell1.setCellType(1);
			cell1.setCellValue("statusType");
			
			cell1 = row.createCell(2);
			cell1.setCellType(1);
			cell1.setCellValue("violate");

			cell1 = row.createCell(3);
			cell1.setCellType(1);
			cell1.setCellValue("support");
			
			cell1 = row.createCell(4);
			cell1.setCellType(1);
			cell1.setCellValue("Confidence");

			cell1 = row.createCell(5);
			cell1.setCellType(1);
			cell1.setCellValue("Cluster");
			
			for (int i=1; i<=rules.length;i++){
				row = sheet.createRow(i);
				
				Cell cell2 = row.createCell(0);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getExpression());

				cell2 = row.createCell(1);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getStatusType());
				
				cell2 = row.createCell(2);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getViolate());

				cell2 = row.createCell(3);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getSupport());
				
				cell2 = row.createCell(4);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getConfidence());
				
				cell2 = row.createCell(5);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getCluster());
			}
				
			FileOutputStream fileOut = new FileOutputStream(filepath);
			wb.write(fileOut);
			fileOut.close();
			wb.close();

		} catch (EncryptedDocumentException|IOException|InvalidFormatException e) {
			e.printStackTrace();
		}
	}

public static void main(String []args){
	String baseFolderPath="AllFiles/";
	String constraintsOutputFilePath=baseFolderPath+"Constraints.txt";
	CPLRule[] constraints= ReadRules.getRulesFromFile(constraintsOutputFilePath);
	String sheetName="z";
	WriteRulesAndConfidence.WriteRulesToExcel(baseFolderPath+"constraints.xlsx", constraints, sheetName) ;
}
	
		
	}
