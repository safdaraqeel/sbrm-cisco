package SBRM.CPLRules;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class ReadRules {

	public static CPLRule[] getRulesFromFile(String filePath) {
		String text = "";
		File fileName = new File(filePath);
		try {
			Scanner scan = new Scanner(fileName);
			boolean startFlag=false;
			boolean endFlag=true;
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (line.contains("---------------")){	
					line = scan.nextLine();
					startFlag=true;
					endFlag=false;
				}
				if (line.contains("Number of Leaves")||line.contains("Number of Rules")||line.contains("): FailedFailed ")||line.contains("): ConnectedConnected")||line.contains("): ConnectedFailed")||line.contains("): FailedConnected")){	
					line = scan.nextLine();
					endFlag=true;
				}
								
				if (startFlag && !endFlag){
				text += line.replaceAll("AND", " AND ").replaceAll("  ", " ")
						.replace(System.getProperty("line.separator"), "");
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// text.replaceAll("AND", " AND ");
		System.out.println(text);
		String[] temp = text.split("\\)");
		CPLRule[] rules = new CPLRule[temp.length];
		for (int i = 0; i < temp.length; i++) {
			rules[i] = new CPLRule();
			String[] temp2 = temp[i].split("\\(");
			if (temp2[0] != null) {
				rules[i].setExpression(temp2[0]);
				if (rules[i].getExpression().contains("ConnectedConnected")) {
					rules[i].setStatusType("ConnectedConnected");
				} else if (rules[i].getExpression().contains("FailedFailed")) {
					rules[i].setStatusType("FailedFailed");
				} else if (rules[i].getExpression().contains("ConnectedFailed")) {
					rules[i].setStatusType("ConnectedFailed");
				} else if (rules[i].getExpression().contains("FailedConnected")) {
					rules[i].setStatusType("FailedConnected");
				} else {
					rules[i].setStatusType("Invalid");
				}
				String [] temp4= rules[i].getExpression().split(":");
				rules[i].setExpression(temp4[0]);
			}
			if (temp2[1] != null && temp2[1].contains("/") == false) {
				rules[i].setSupport(Double.parseDouble(temp2[1]));
				rules[i].setViolate(0);
				if (rules[i].getSupport()+rules[i].getViolate()==0){
					rules[i].setConfidence(0);;
				}
				else {
					rules[i].setConfidence(1);
				}
			}

			if (temp2[1] != null && temp2[1].contains("/") == true) {
				String[] temp3 = temp2[1].split("/");
				rules[i].setSupport(Double.parseDouble(temp3[0]));
				rules[i].setViolate(Double.parseDouble(temp3[1]));
				double conf= (rules[i].getSupport()-rules[i].getViolate())/(rules[i].getSupport()+rules[i].getViolate());
				rules[i].setConfidence(conf);
			}
		}
		return rules;
	}

	public static CPLRule[] getRulesFromXLFile()  {
		CPLRule[] rules = null;
		try {
			List<String> selectedAlgos  = Files.readAllLines(Paths.get("src/main/resources/SAs.text"), Charset.defaultCharset());

			String filePath = "constraints-" + selectedAlgos.get(0) + "-" + selectedAlgos.get(1) + ".xlsx";
			File myFile = new File(filePath);
			if (!myFile.exists()){
				myFile = new File("ASE_AllFiles/ExperimentData/ZeroIteration/" + selectedAlgos.get(1) + "/InitialConstraints.xlsx");
			}

			FileInputStream fis;
			fis = new FileInputStream(myFile);
			XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
			XSSFSheet mySheet = myWorkBook.getSheetAt(myWorkBook.getNumberOfSheets()-1);
			Iterator<Row> rowIterator = mySheet.iterator();
			rules = new CPLRule[mySheet.getPhysicalNumberOfRows() - 2];

			for (int i = 1; i < mySheet.getPhysicalNumberOfRows()-1; i++) {
				rules[i-1] = new CPLRule();
				Row row = mySheet.getRow(i);
				rules[i-1].setExpression(row.getCell(0).getStringCellValue());
				rules[i-1].setStatusType(row.getCell(1).getStringCellValue());
				rules[i-1].setViolate(row.getCell(2).getNumericCellValue());
				rules[i-1].setSupport(row.getCell(3).getNumericCellValue());
				rules[i-1].setConfidence(row.getCell(4).getNumericCellValue());
				rules[i-1].setCluster(row.getCell(5).getStringCellValue());
				rules[i-1].printRule();
			}
			return rules;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return rules;

	}

}
