package SBRM.CG;
import SBRM.CPLRules.CPLRule;
import SBRM.CPLRules.ReadRules;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.*;
import jmetal.util.JMException;
import jmetal.util.wrapper.XInt;

/**
 * Class representing problem ZDT1
 */
public class ConfigProblem extends Problem {
public CPLRule[] rules;
	
 /** 
  * Constructor.
  * Creates a default instance of problem ZDT1 (30 decision variables)
  * @param solutionType The solution type must "Real", "BinaryReal, and "ArrayReal". 
	* ArrayReal, or ArrayRealC".
  */
  public ConfigProblem(String solutionType) throws ClassNotFoundException {
    this(solutionType, numberOfVariables_); 
	
  } // ZDT1
  
	public ConfigProblem(String solutionType, Integer numberOfVariables) {
		ReadRules readRules = new ReadRules();
		//this.constraints = readRules.getConstraintsFromFile();
		this.rules = readRules.getRulesFromXLFile();
		
		numberOfVariables_ = numberOfVariables;
		numberOfObjectives_ = 3;
		numberOfConstraints_ = 0;
		problemName_ = "ConfigProblem";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];

		// Establishes upper and lower limits for the variables
		for (int var = 0; var < numberOfVariables_; var++) {
			if (var==0 || var==1 || var==2) {
				lowerLimit_[var] = 0;
				upperLimit_[var] = 2;
			}else if (var==3 || var==4 || var==5) {
				lowerLimit_[var] = 0;
				upperLimit_[var] = 1;
			} else if (var == 6 || var == 7 || var == 8 || var == 9 || var == 10 || var == 11) {
				lowerLimit_[var] = 0;
				upperLimit_[var] = 3;
			} else if ( var == 12 || var == 13 || var == 14) {
				lowerLimit_[var] = 0;
				upperLimit_[var] = 2;

			} else if (var == 15 || var == 16 || var == 17) {
				lowerLimit_[var] = 576;
				upperLimit_[var] = 1500;
			} else {
				lowerLimit_[var] = 64;
				upperLimit_[var] = 6000;
			}
    } // for

    if (solutionType.compareTo("BinaryReal") == 0)
    	solutionType_ = new BinaryRealSolutionType(this) ;
    else if (solutionType.compareTo("Real") == 0) 
    	solutionType_ = new RealSolutionType(this) ;
    else if (solutionType.compareTo("ArrayReal") == 0)
    	solutionType_ = new ArrayRealSolutionType(this) ;
    else if (solutionType.compareTo("Integer") == 0)
    	solutionType_ = new IntSolutionType(this) ;
    else if (solutionType.compareTo("IntegerArray") == 0)
    	solutionType_ = new ArrayIntSolutionType(this) ;
    else {
    	System.out.println("Error: solution type " + solutionType + " invalid") ;
    	System.exit(-1) ;
    }
  } // ConfigProblem
    
  /** 
   * Evaluates a solution.
   * @param solution The solution to evaluate.
   * @throws JMException
   */
  public void evaluate(Solution solution) throws JMException {
		XInt x = new XInt(solution) ;
		
    double [] f = new double[numberOfObjectives_]  ;
    double f1=this.FitnessForExploringAwayFromHighConfidenceConnected(x);
    double f2=this.FitnessForExploringNearFailed(x);
    double f3=this.FitnessForExploringNearLowConfidenceConnected(x);
    f[0]=f1;
    f[1]=f2;
    f[2]=f3;
    solution.setObjective(0,f[0]);
    solution.setObjective(1,f[1]);
    solution.setObjective(2,f[2]);
  } // evaluate

  private double FitnessForExploringAwayFromHighConfidenceConnected(XInt x) throws JMException {
		double fitness=0;
			// intializing a configuraiton object with solution
			EncodedConfigurableParameters configuration = new EncodedConfigurableParameters();
			configuration = configuration.intializeConfigurationInstance(x);
			// evaluating constraints using solution
			DistanceCalculation disObject= new DistanceCalculation();
			this.rules=disObject.CalculateDistanceForAllRules(rules,configuration);
			// calculating the fitness
			fitness= Objective1.calculateFitnessForAwayFromConnected(this.rules, configuration);
		return fitness;
	}

  private double FitnessForExploringNearFailed(XInt x) throws JMException {
		double fitness=0;
			// intializing a configuraiton object with solution
			EncodedConfigurableParameters configuration = new EncodedConfigurableParameters();
			configuration = configuration.intializeConfigurationInstance(x);
			fitness= Objective2.calculateFitnessForNearFailed(this.rules, configuration);
		return fitness;
	  }

  private double FitnessForExploringNearLowConfidenceConnected(XInt x) throws JMException {
		double fitness=0;
			// intializing a configuraiton object with solution
	  		EncodedConfigurableParameters configuration = new EncodedConfigurableParameters();
			configuration = configuration.intializeConfigurationInstance(x);
			fitness= Objective3.calculateFitnessForNearConnectedLowConfidence(this.rules, configuration);
		return fitness;
	  }
}