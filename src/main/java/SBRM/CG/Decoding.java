package SBRM.CG;

import java.io.*;
import java.util.Scanner;

public class Decoding {

	public static void ReadData(String sourcePath, String destinationPath) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(destinationPath, true));
		writer.write("Encryption; Encryption_s2; Encryption_s3;ListenPort;ListenPort_s2;ListenPort_s3;DefaultCall_Protocol;DefaultCall_Protocol_s2;DefaultCall_Protocol_s3;DefaultTransport;DefaultTransport_s2;DefaultTransport_s3;Ice_DefaultCandidate;Ice_DefaultCandidate_s2;Ice_DefaultCandidate_S3;MTU;MTU_s2;MTU_s3;DefaultCall_Rate;DefaultCall_Rate_s2;DefaultCall_Rate_s3;MaxTransmitCallRate;MaxTransmitCallRate_s2;MaxTransmitCallRate_s3;MaxReceiveCallRate;MaxReceiveCallRate_s2;MaxReceiveCallRate_s3;Done\n");

		String filePath = sourcePath;
		String text = "";
		File fileName = new File(filePath);
		try {
			Scanner scan = new Scanner(fileName);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] Variables = line.split(" ");
				System.out.println(line);
				//column 1-4
				if (Variables[0].contains("0")){
					writer.write("Off;");
				}if (Variables[0].contains("1")){
					writer.write("On;");
				}if (Variables[0].contains("2")){
					writer.write("BestEffort;");

				}if (Variables[1].contains("0")){
					writer.write("Off;");
				}if (Variables[1].contains("1")){
					writer.write("On;");
				}if (Variables[1].contains("2")){
					writer.write("BestEffort;");

				}if (Variables[2].contains("0")){
					writer.write("Off;");
				}if (Variables[2].contains("1")){
					writer.write("On;");
				}if (Variables[2].contains("2")){
					writer.write("BestEffort;");

				}if (Variables[3].contains("0")){
					writer.write("Off;");
				}if (Variables[3].contains("1")){
					writer.write("On;");

				}if (Variables[4].contains("0")){
					writer.write("Off;");
				}if (Variables[4].contains("1")){
					writer.write("On;");

				}if (Variables[5].contains("0")){
					writer.write("Off;");
				}if (Variables[5].contains("1")){
					writer.write("On;");

				}if (Variables[6].contains("0")){
					writer.write("Auto;");
				}if (Variables[6].contains("1")){
					writer.write("Sip;");
				}if (Variables[6].contains("2")){
					writer.write("H323;");
				}if (Variables[6].contains("3")){
					writer.write("H320;");

				}if (Variables[7].contains("0")){
					writer.write("Auto;");
				}if (Variables[7].contains("1")){
					writer.write("Sip;");
				}if (Variables[7].contains("2")){
					writer.write("H323;");
				}if (Variables[7].contains("3")){
					writer.write("H320;");

				}if (Variables[8].contains("0")){
					writer.write("Auto;");
				}if (Variables[8].contains("1")){
					writer.write("Sip;");
				}if (Variables[8].contains("2")){
					writer.write("H323;");
				}if (Variables[8].contains("3")){
					writer.write("H320;");

				}if (Variables[9].contains("0")){
					writer.write("Auto;");
				}if (Variables[9].contains("1")){
					writer.write("TCP;");
				}if (Variables[9].contains("2")){
					writer.write("Tls;");
				}if (Variables[9].contains("3")){
					writer.write("UDP;");

				}if (Variables[10].contains("0")){
					writer.write("Auto;");
				}if (Variables[10].contains("1")){
					writer.write("TCP;");
				}if (Variables[10].contains("2")){
					writer.write("Tls;");
				}if (Variables[10].contains("3")){
					writer.write("UDP;");

				}if (Variables[11].contains("0")){
					writer.write("Auto;");
				}if (Variables[11].contains("1")){
					writer.write("TCP;");
				}if (Variables[11].contains("2")){
					writer.write("Tls;");
				}if (Variables[11].contains("3")){
					writer.write("UDP;");

				}if (Variables[12].contains("0")){
					writer.write("Host;");
				}if (Variables[12].contains("1")){
					writer.write("Relay;");
				}if (Variables[12].contains("2")){
					writer.write("Rflx;");

				}if (Variables[13].contains("0")){
					writer.write("Host;");
				}if (Variables[13].contains("1")){
					writer.write("Relay;");
				}if (Variables[13].contains("2")){
					writer.write("Rflx;");

				}if (Variables[14].contains("0")){
					writer.write("Host;");
				}if (Variables[14].contains("1")){
					writer.write("Relay;");
				}if (Variables[14].contains("2")){
					writer.write("Rflx;");
				}

				writer.write(Variables[15]+";");
				writer.write(Variables[16]+";");
				writer.write(Variables[17]+";");
				writer.write(Variables[18]+";");
				writer.write(Variables[19]+";");
				writer.write(Variables[20]+";");
				writer.write(Variables[21]+";");
				writer.write(Variables[22]+";");
				writer.write(Variables[23]+";");
				writer.write(Variables[24]+";");
				writer.write(Variables[25]+";");
				writer.write(Variables[26]+";");
//				writer.write(Variables[17]+";");
				writer.write("NotDone\n");

			}
			writer.flush();
			writer.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
    	ReadData("VAR","VAR.csv") ;
	}
}
