package SBRM.CG;

import SBRM.CPLRules.CPLRule;

public class Objective1 {
    public static double calculateFitnessForAwayFromConnected(CPLRule[] rules, EncodedConfigurableParameters configuration){
        double sumOfWeightedDistance=0;
        double maxWeightedDistance=0;
        boolean flag=false;

        for (int i=0; i<rules.length;i++){
            if (rules[i].getStatusType().contains("ConnectedConnected") && rules[i].getCluster().contains("High")){
                //System.out.println(rules[i].getExpression()+" {Distance: "+rules[i].getDistance()+"}");
                double weightedDistance = (rules[i].getConfidence()*rules[i].getDistance());
                sumOfWeightedDistance += weightedDistance;
                flag=true;
            }// end of if
            //calculating the maximum weight by add all weights for all connected status rules
            if (rules[i].getStatusType().contains("ConnectedConnected")){
                maxWeightedDistance+=rules[i].getConfidence();
            }// end of if
        }//end of for
        //System.out.println("sumOfWeightedDistance is:- "+sumOfWeightedDistance);
        //System.out.println("maxWeightedDistance is:- "+maxWeightedDistance);
        double normalizedSumOfWeightedDistance = (sumOfWeightedDistance/maxWeightedDistance);	//(x-min/max-min) normalizaation function
        //double normalizedSumOfWeightedDistance = sumOfWeightedDistance/(sumOfWeightedDistance+1); //(x/x+1) normalizaation function
        //System.out.println("normalizedSumOfWeightedDistance is:- "+normalizedSumOfWeightedDistance);
        double fitnessValue = 1-normalizedSumOfWeightedDistance;
        System.out.println("fitness value for exploring away from the normal state rules is:- "+fitnessValue);

        if (flag)
            return fitnessValue;
        else
            return 0.5;

    }

}
