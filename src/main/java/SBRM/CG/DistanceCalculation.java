package SBRM.CG;

import SBRM.CPLRules.CPLRule;

public class DistanceCalculation {

	// for normalization
	public double nor(double x) {
		return x / (x + 1);
	}

	// for x==y
	public double xEqualsY(double k, double x, double y) {
		// for categorical variables
		if (x < 5) {
			if (x - y == 0) {
				return 0;
			} else {
				return k * nor(Math.abs(x - y));
			}
		}
		// for numeric variables
		else {

			if (x - y == 0) {
				return 0;
			} else {
				return k * nor(Math.abs(x - y));
			}

		}

	}

	// for x!=y
	public double xNotEqualsY(double k, double x, double y) {

		if (x - y != 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x<y
	public double xLessThanY(double k, double x, double y) {

		if (x - y < 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x<=y
	public double xLessEqualsThanY(double k, double x, double y) {

		if (x - y <= 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x>y
	public double xGreaterThanY(double k, double x, double y) {

		if (y - x < 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x>=y
	public double xGreaterEqualsThanY(double k, double x, double y) {

		if (y - x <= 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	public CPLRule[] CalculateDistanceForAllRules(CPLRule [] rules, EncodedConfigurableParameters configuration){
		int maximumClausesInAnyRule=0;
		double [] distance = new double [rules.length];
		//System.out.println("This is parsing rules and calculating distance");
		DistanceCalculation disObj= new DistanceCalculation();
		for (int k=0;k<rules.length;k++){
			String [] clauses =rules[k].getExpression().split("AND");
			if (clauses.length>maximumClausesInAnyRule){
				maximumClausesInAnyRule=clauses.length;
			}
		}

		for (int i=0;i<rules.length;i++){
			String [] clauses =rules[i].getExpression().split("AND");
			for (int j=0;j<clauses.length;j++){

				//System.out.println("Rule#"+i+" and clause#"+j+" "+clauses[j]);
				//***********Replacing values and variables in rules with numeric number from the configuration data generated ****************
				if (clauses[j].contains("Encryption_s3")||clauses[j].contains("Encryption_s2")||clauses[j].contains("Encryption")){
					clauses[j]=clauses[j].replace("Encryption_s3", Double.toString(configuration.getEncryption_s3()));
					clauses[j]=clauses[j].replace("Encryption_s2", Double.toString(configuration.getEncryption_s2()));
					clauses[j]=clauses[j].replace("Encryption", Double.toString(configuration.getEncryption()));
					clauses[j]=clauses[j].replace("Off", "0");
					clauses[j]=clauses[j].replace("On", "1");
					clauses[j]=clauses[j].replace("BestEffort", "2");
				}
				if (clauses[j].contains("ListenPort_s3")||clauses[j].contains("ListenPort_s2")||clauses[j].contains("ListenPort")){
					clauses[j]=clauses[j].replace("ListenPort_s3", Double.toString(configuration.getListenPort_s3()));
					clauses[j]=clauses[j].replace("ListenPort_s2", Double.toString(configuration.getListenPort_s2()));
					clauses[j]=clauses[j].replace("ListenPort", Double.toString(configuration.getListenPort()));
					clauses[j]=clauses[j].replace("Off", "0");
					clauses[j]=clauses[j].replace("On", "1");
				}
				if (clauses[j].contains("DefaultCall_Protocol_s3")||clauses[j].contains("DefaultCall_Protocol_s2")||clauses[j].contains("DefaultCall_Protocol")){
					clauses[j]=clauses[j].replace("DefaultCall_Protocol_s3", Double.toString(configuration.getDefaultCall_Protocol_s3()));
					clauses[j]=clauses[j].replace("DefaultCall_Protocol_s2", Double.toString(configuration.getDefaultCall_Protocol_s2()));
					clauses[j]=clauses[j].replace("DefaultCall_Protocol", Double.toString(configuration.getDefaultCall_Protocol()));
					clauses[j]=clauses[j].replace("Auto", "0");
					clauses[j]=clauses[j].replace("Sip", "1");
					clauses[j]=clauses[j].replace("H323", "2");
					clauses[j]=clauses[j].replace("H320", "3");
				}
//			if (clauses[j].contains("DefaultCall_Protocol")){
//				clauses[j]=clauses[j].replace("DefaultCall_Protocol", Double.toString(configuration.getDefaultCall_Protocol()));
//				clauses[j]=clauses[j].replace("Auto", "0");
//				clauses[j]=clauses[j].replace("Sip", "1");
//				clauses[j]=clauses[j].replace("H323", "2");
//				clauses[j]=clauses[j].replace("H320", "3");
//			}
				if (clauses[j].contains("DefaultTransport_s3")||clauses[j].contains("DefaultTransport_s2")||clauses[j].contains("DefaultTransport")){
					clauses[j]=clauses[j].replace("DefaultTransport_s3", Double.toString(configuration.getDefaultTransport_s3()));
					clauses[j]=clauses[j].replace("DefaultTransport_s2", Double.toString(configuration.getDefaultTransport_s2()));
					clauses[j]=clauses[j].replace("DefaultTransport", Double.toString(configuration.getDefaultTransport()));
					clauses[j]=clauses[j].replace("Auto", "0");
					clauses[j]=clauses[j].replace("TCP", "1");
					clauses[j]=clauses[j].replace("Tls", "2");
					clauses[j]=clauses[j].replace("UDP", "3");
				}
				if (clauses[j].contains("Ice_DefaultCandidate_s3")||clauses[j].contains("Ice_DefaultCandidate_s2")||clauses[j].contains("Ice_DefaultCandidate")){
					clauses[j]=clauses[j].replace("Ice_DefaultCandidate_s3", Double.toString(configuration.getIce_DefaultCandidate_s3()));
					clauses[j]=clauses[j].replace("Ice_DefaultCandidate_s2", Double.toString(configuration.getIce_DefaultCandidate_s2()));
					clauses[j]=clauses[j].replace("Ice_DefaultCandidate", Double.toString(configuration.getIce_DefaultCandidate()));
					clauses[j]=clauses[j].replace("Host", "0");
					clauses[j]=clauses[j].replace("Relay", "1");
					clauses[j]=clauses[j].replace("Rflx", "2");
				}
				//***********Replacing values in rules from geneared configuraiton ****************
				clauses[j]=clauses[j].replace("MTU_s3",  Double.toString(configuration.getMTU_s3()));
				clauses[j]=clauses[j].replace("MTU_s2",  Double.toString(configuration.getMTU_s2()));
				clauses[j]=clauses[j].replace("MTU",  Double.toString(configuration.getMTU()));

				clauses[j]=clauses[j].replace("DefaultCall_Rate_s3",  Double.toString(configuration.getDefaultCall_Rate_s3()));
				clauses[j]=clauses[j].replace("DefaultCall_Rate_s2",  Double.toString(configuration.getDefaultCall_Rate_s2()));
				clauses[j]=clauses[j].replace("DefaultCall_Rate",  Double.toString(configuration.getDefaultCall_Rate()));

				clauses[j]=clauses[j].replace("MaxTransmitCallRate_s3",  Double.toString(configuration.getMaxTransmitCallRate_s3()));
				clauses[j]=clauses[j].replace("MaxTransmitCallRate_s2",  Double.toString(configuration.getMaxTransmitCallRate_s2()));
				clauses[j]=clauses[j].replace("MaxTransmitCallRate",  Double.toString(configuration.getMaxTransmitCallRate()));

				clauses[j]=clauses[j].replace("MaxReceiveCallRate_s3",  Double.toString(configuration.getMaxReceiveCallRate_s3()));
				clauses[j]=clauses[j].replace("MaxReceiveCallRate_s2",  Double.toString(configuration.getMaxReceiveCallRate_s2()));
				clauses[j]=clauses[j].replace("MaxReceiveCallRate",  Double.toString(configuration.getMaxReceiveCallRate()));
				//System.out.println("Rule#"+i+" and clause#"+j+" "+clauses[j]);

				if (clauses[j].contains("<=")){
					//	System.out.println("<= operator found");
					String [] oprands = clauses[j].split("<=");
					//	System.out.println(oprands[0]+" <= " + oprands[1]);

					distance[i]+= disObj.xLessEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
					//	System.out.println("distance is:- "+disObj.xLessEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
				}
				else if (clauses[j].contains(">=")){
					//System.out.println(">= operator found");
					String [] oprands = clauses[j].split(">=");
					//System.out.println(oprands[0]+" >= " + oprands[1]);

					distance[i]+= disObj.xGreaterEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
					//	System.out.println("distance is:- "+disObj.xGreaterEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
				}
				else if (clauses[j].contains("<")){
					//System.out.println("< operator found");
					String [] oprands = clauses[j].split("<");
					//System.out.println(oprands[0]+" < " + oprands[1]);

					distance[i]+= disObj.xLessThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
					//	System.out.println("distance is:- "+disObj.xLessThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
				}
				else if (clauses[j].contains(">")){
					//System.out.println("> operator found");
					String [] oprands = clauses[j].split(">");
					//System.out.println(oprands[0]+" > " + oprands[1]);
					distance[i]+= disObj.xGreaterThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
					//	System.out.println("distance is:- "+disObj.xGreaterThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
				}

				else if (clauses[j].contains("=")){
					//System.out.println("= operator found");
					String [] oprands = clauses[j].split("=");
					//System.out.println(oprands[0]+" = " + oprands[1]);
					distance[i]+= disObj.xEqualsY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
					//		System.out.println("distance is:- "+disObj.xEqualsY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
				}
				else {
					System.out.println("No operator found");
				}
			}
			rules[i].setDistance((distance[i]/maximumClausesInAnyRule));
		}
		return rules;



	}



}
