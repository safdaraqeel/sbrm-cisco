package SBRM.CG;

import jmetal.util.JMException;
import jmetal.util.wrapper.XInt;

import java.util.Random;

public class EncodedConfigurableParameters {
    public int Encryption;
    public int Encryption_s2;
    public int Encryption_s3;

    public int ListenPort;
    public int ListenPort_s2;
    public int ListenPort_s3;

    public int DefaultCall_Protocol;
    public int DefaultCall_Protocol_s2;
    public int DefaultCall_Protocol_s3;

    public int DefaultTransport;
    public int DefaultTransport_s2;
    public int DefaultTransport_s3;

    public int Ice_DefaultCandidate;
    public int Ice_DefaultCandidate_s2;
    public int Ice_DefaultCandidate_s3;

    public int MTU;
    public int MTU_s2;
    public int MTU_s3;

    public int DefaultCall_Rate;
    public int DefaultCall_Rate_s2;
    public int DefaultCall_Rate_s3;

    public int MaxTransmitCallRate;
    public int MaxTransmitCallRate_s2;
    public int MaxTransmitCallRate_s3;

    public int MaxReceiveCallRate;
    public int MaxReceiveCallRate_s2;
    public int MaxReceiveCallRate_s3;

    public static int randInt(int min, int max) {
        Random rand = new Random(System.nanoTime()*System.currentTimeMillis());
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public int getEncryption() {
        return Encryption;
    }

    public void setEncryption(int encryption) {
        Encryption = encryption;
    }

    public int getEncryption_s2() {
        return Encryption_s2;
    }

    public void setEncryption_s2(int encryption_s2) {
        Encryption_s2 = encryption_s2;
    }

    public int getEncryption_s3() {
        return Encryption_s3;
    }

    public void setEncryption_s3(int encryption_s3) {
        Encryption_s3 = encryption_s3;
    }

    public int getMTU() {
        return MTU;
    }

    public void setMTU(int MTU) {
        this.MTU = MTU;
    }

    public int getMTU_s2() {
        return MTU_s2;
    }

    public void setMTU_s2(int MTU_s2) {
        this.MTU_s2 = MTU_s2;
    }

    public int getMTU_s3() {
        return MTU_s3;
    }

    public void setMTU_s3(int MTU_s3) {
        this.MTU_s3 = MTU_s3;
    }

    public int getMaxTransmitCallRate() {
        return MaxTransmitCallRate;
    }

    public void setMaxTransmitCallRate(int maxTransmitCallRate) {
        MaxTransmitCallRate = maxTransmitCallRate;
    }

    public int getMaxTransmitCallRate_s2() {
        return MaxTransmitCallRate_s2;
    }

    public void setMaxTransmitCallRate_s2(int maxTransmitCallRate_s2) {
        MaxTransmitCallRate_s2 = maxTransmitCallRate_s2;
    }

    public int getMaxTransmitCallRate_s3() {
        return MaxTransmitCallRate_s3;
    }

    public void setMaxTransmitCallRate_s3(int maxTransmitCallRate_s3) {
        MaxTransmitCallRate_s3 = maxTransmitCallRate_s3;
    }

    public int getMaxReceiveCallRate() {
        return MaxReceiveCallRate;
    }

    public void setMaxReceiveCallRate(int maxReceiveCallRate) {
        MaxReceiveCallRate = maxReceiveCallRate;
    }

    public int getMaxReceiveCallRate_s2() {
        return MaxReceiveCallRate_s2;
    }

    public void setMaxReceiveCallRate_s2(int maxReceiveCallRate_s2) {
        MaxReceiveCallRate_s2 = maxReceiveCallRate_s2;
    }

    public int getMaxReceiveCallRate_s3() {
        return MaxReceiveCallRate_s3;
    }

    public void setMaxReceiveCallRate_s3(int maxReceiveCallRate_s3) {
        MaxReceiveCallRate_s3 = maxReceiveCallRate_s3;
    }

    public int getListenPort() {
        return ListenPort;
    }

    public void setListenPort(int listenPort) {
        ListenPort = listenPort;
    }

    public int getListenPort_s2() {
        return ListenPort_s2;
    }

    public void setListenPort_s2(int listenPort_s2) {
        ListenPort_s2 = listenPort_s2;
    }

    public int getListenPort_s3() {
        return ListenPort_s3;
    }

    public void setListenPort_s3(int listenPort_s3) {
        ListenPort_s3 = listenPort_s3;
    }

    public int getDefaultCall_Protocol() {
        return DefaultCall_Protocol;
    }

    public void setDefaultCall_Protocol(int defaultCall_Protocol) {
        DefaultCall_Protocol = defaultCall_Protocol;
    }

    public int getDefaultCall_Protocol_s2() {
        return DefaultCall_Protocol_s2;
    }

    public void setDefaultCall_Protocol_s2(int defaultCall_Protocol_s2) {
        DefaultCall_Protocol_s2 = defaultCall_Protocol_s2;
    }

    public int getDefaultCall_Protocol_s3() {
        return DefaultCall_Protocol_s3;
    }

    public void setDefaultCall_Protocol_s3(int defaultCall_Protocol_s3) {
        DefaultCall_Protocol_s3 = defaultCall_Protocol_s3;
    }

    public int getDefaultTransport() {
        return DefaultTransport;
    }

    public void setDefaultTransport(int defaultTransport) {
        DefaultTransport = defaultTransport;
    }

    public int getDefaultTransport_s2() {
        return DefaultTransport_s2;
    }

    public void setDefaultTransport_s2(int defaultTransport_s2) {
        DefaultTransport_s2 = defaultTransport_s2;
    }

    public int getDefaultTransport_s3() {
        return DefaultTransport_s3;
    }

    public void setDefaultTransport_s3(int defaultTransport_s3) {
        DefaultTransport_s3 = defaultTransport_s3;
    }

    public int getIce_DefaultCandidate() {
        return Ice_DefaultCandidate;
    }

    public void setIce_DefaultCandidate(int ice_DefaultCandidate) {
        Ice_DefaultCandidate = ice_DefaultCandidate;
    }

    public int getIce_DefaultCandidate_s2() {
        return Ice_DefaultCandidate_s2;
    }

    public void setIce_DefaultCandidate_s2(int ice_DefaultCandidate_s2) {
        Ice_DefaultCandidate_s2 = ice_DefaultCandidate_s2;
    }

    public int getIce_DefaultCandidate_s3() {
        return Ice_DefaultCandidate_s3;
    }

    public void setIce_DefaultCandidate_s3(int ice_DefaultCandidate_s3) {
        Ice_DefaultCandidate_s3 = ice_DefaultCandidate_s3;
    }

    public int getDefaultCall_Rate() {
        return DefaultCall_Rate;
    }

    public void setDefaultCall_Rate(int defaultCall_Rate) {
        DefaultCall_Rate = defaultCall_Rate;
    }

    public int getDefaultCall_Rate_s2() {
        return DefaultCall_Rate_s2;
    }

    public void setDefaultCall_Rate_s2(int defaultCall_Rate_s2) {
        DefaultCall_Rate_s2 = defaultCall_Rate_s2;
    }

    public int getDefaultCall_Rate_s3() {
        return DefaultCall_Rate_s3;
    }

    public void setDefaultCall_Rate_s3(int defaultCall_Rate_s3) {
        DefaultCall_Rate_s3 = defaultCall_Rate_s3;
    }

    public EncodedConfigurableParameters getRandomConfiguration(){
        int num = randInt(0, 2);
        this.setEncryption(num);
        int num2 = randInt(0, 2);
        this.setEncryption_s2(num2);
        int num3 = randInt(0, 2);
        this.setEncryption_s3(num3);
        num3 = randInt(0, 2);

        num = randInt(0, 1);
        this.setListenPort(num);
        num2 = randInt(0, 1);
        this.setListenPort_s2(num2);
        num3 = randInt(0, 1);
        this.setListenPort_s3(num3);

        num = randInt(0, 3);
        this.setDefaultCall_Protocol(num);
        num2 = randInt(0, 3);
        this.setDefaultCall_Protocol_s2(num2);
        num2 = randInt(0, 3);
        this.setDefaultCall_Protocol_s3(num3);

        num = randInt(0, 3);
        this.setDefaultTransport(num);
        num2 = randInt(0, 3);
        this.setDefaultTransport_s2(num2);
        num3 = randInt(0, 3);
        this.setDefaultTransport_s3(num3);

        num = randInt(0, 2);
        this.setIce_DefaultCandidate(num);
        num2 = randInt(0, 2);
        this.setIce_DefaultCandidate_s2(num2);
        num3 = randInt(0, 2);
        this.setIce_DefaultCandidate_s3(num3);

        num = randInt(576, 1500);
        this.setMTU(num);
        num2 = randInt(576, 1500);
        this.setMTU_s2(num2);
        num3 = randInt(576, 1500);
        this.setMTU_s3(num3);

        num = randInt(64, 6000);
        this.setDefaultCall_Rate(num);
        num2 = randInt(64, 6000);
        this.setDefaultCall_Rate_s2(num2);
        num3 = randInt(64, 6000);
        this.setDefaultCall_Rate_s3(num3);

        num = randInt(64, 6000);
        this.setMaxTransmitCallRate(num);
        num2 = randInt(64, 6000);
        this.setMaxTransmitCallRate_s2(num2);
        num3 = randInt(64, 6000);
        this.setMaxTransmitCallRate_s3(num3);

        num = randInt(64, 6000);
        this.setMaxReceiveCallRate(num);
        num2 = randInt(64, 6000);
        this.setMaxReceiveCallRate_s2(num2);
        num3 = randInt(64, 6000);
        this.setMaxReceiveCallRate_s3(num3);

        return this;
    }



    public EncodedConfigurableParameters intializeConfigurationInstance(XInt x) throws JMException {
        this.Encryption = x.getValue(0);
        this.Encryption_s2 = x.getValue(1);
        this.Encryption_s3 = x.getValue(2);
        this.ListenPort = x.getValue(3);
        this.ListenPort_s2 = x.getValue(4);
        this.ListenPort_s3 = x.getValue(5);
        this.DefaultCall_Protocol = x.getValue(6);
        this.DefaultCall_Protocol_s2 = x.getValue(7);
        this.DefaultCall_Protocol_s3 = x.getValue(8);
        this.DefaultTransport = x.getValue(9);
        this.DefaultTransport_s2 = x.getValue(10);
        this.DefaultTransport_s3 = x.getValue(11);
        this.Ice_DefaultCandidate = x.getValue(12);
        this.Ice_DefaultCandidate_s2 = x.getValue(13);
        this.Ice_DefaultCandidate_s3 = x.getValue(14);
        this.MTU = x.getValue(15);
        this.MTU_s2 = x.getValue(16);
        this.MTU_s3 = x.getValue(17);
        this.DefaultCall_Rate = x.getValue(18);
        this.DefaultCall_Rate_s2 = x.getValue(19);
        this.DefaultCall_Rate_s3 = x.getValue(20);
        this.MaxTransmitCallRate = x.getValue(21);
        this.MaxTransmitCallRate_s2 = x.getValue(22);
        this.MaxTransmitCallRate_s3 = x.getValue(23);
        this.MaxReceiveCallRate = x.getValue(24);
        this.MaxReceiveCallRate_s2 = x.getValue(25);
        this.MaxReceiveCallRate_s3 = x.getValue(26);
        return this;
    }
}




