package SBRM.CG;

import SBRM.CPLRules.CPLRule;

public class Objective2 {

    public static double calculateFitnessForNearFailed(CPLRule[] rules, EncodedConfigurableParameters configuration){
        double sumOfWeightedDistance=0;
        double maxWeightedDistance=0;
        double confidence=0;
        for (int i=0; i<rules.length;i++){
            //double violationProbability= rules[i].getViolate() / (rules[i].getSupport()+rules[i].getViolate());
            if (rules[i].getSupport()+rules[i].getViolate()!=0){
                confidence = (rules[i].getSupport()-rules[i].getViolate())/ (rules[i].getSupport()+rules[i].getViolate());
            }

            if (rules[i].getStatusType().contains("Failed")){
                //System.out.println(rules[i].getExpression()+" {Distance: "+rules[i].getDistance()+"}");
                double weightedDistance = (confidence* (1-rules[i].getDistance()));
                sumOfWeightedDistance += weightedDistance;
            }// end of if
            //calculating the maximum weight by add all weights for all connected status rules
            if (rules[i].getStatusType().contains("Failed")){
                maxWeightedDistance+=confidence;
            }// end of if
        }//end of for
        //System.out.println("sumOfWeightedDistance is:- "+sumOfWeightedDistance);
        //System.out.println("maxWeightedDistance is:- "+maxWeightedDistance);
        double normalizedSumOfWeightedDistance = (sumOfWeightedDistance/maxWeightedDistance);	//(x-min/max-min) normalizaation function
        //double normalizedSumOfWeightedDistance = sumOfWeightedDistance/(sumOfWeightedDistance+1); //(x/x+1) normalizaation function
        //System.out.println("normalizedSumOfWeightedDistance is:- "+normalizedSumOfWeightedDistance);
        double fitnessValue = 1-normalizedSumOfWeightedDistance;
        System.out.println("fitness value for exploring near abnormal state rules is:- "+fitnessValue);
        return fitnessValue;
    }

}
