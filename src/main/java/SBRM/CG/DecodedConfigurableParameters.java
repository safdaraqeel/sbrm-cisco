package SBRM.CG;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class DecodedConfigurableParameters {
    public String Encryption;
    public String ListenPort;
    public String DefaultCall_Protocol;
    public String DefaultTransport;
    public String Ice_DefaultCandidate;
    public int MTU;
    public int DefaultCall_Rate;
    public int MaxTransmitCallRate;
    public int MaxReceiveCallRate;


    public String getEncryption() {
        return Encryption;
    }

    public void setEncryption(String encryption) {
        Encryption = encryption;
    }

    public String getListenPort() {
        return ListenPort;
    }

    public void setListenPort(String listenPort) {
        ListenPort = listenPort;
    }

    public String getDefaultCall_Protocol() {
        return DefaultCall_Protocol;
    }

    public void setDefaultCall_Protocol(String defaultCall_Protocol) {
        DefaultCall_Protocol = defaultCall_Protocol;
    }

    public String getDefaultTransport() {
        return DefaultTransport;
    }

    public void setDefaultTransport(String DefaultTransport) {
        DefaultTransport = DefaultTransport;
    }

    public String getIce_DefaultCandidate() {
        return Ice_DefaultCandidate;
    }

    public void setIce_DefaultCandidate(String ice_DefaultCandidate) {
        Ice_DefaultCandidate = ice_DefaultCandidate;
    }

    public int getMTU() {
        return MTU;
    }

    public void setMTU(int MTU) {
        this.MTU = MTU;
    }

    public int getDefaultCall_Rate() {
        return DefaultCall_Rate;
    }

    public void setDefaultCall_Rate(int defaultCall_Rate) {
        DefaultCall_Rate = defaultCall_Rate;
    }

    public int getMaxTransmitCallRate() {
        return MaxTransmitCallRate;
    }

    public void setMaxTransmitCallRate(int maxTransmitCallRate) {
        MaxTransmitCallRate = maxTransmitCallRate;
    }

    public int getMaxReceiveCallRate() {
        return MaxReceiveCallRate;
    }

    public void setMaxReceiveCallRate(int maxReceiveCallRate) {
        MaxReceiveCallRate = maxReceiveCallRate;
    }



    public static DecodedConfigurableParameters getDefaultConfigurations() {
        DecodedConfigurableParameters receiverConfiguration = new DecodedConfigurableParameters();
        receiverConfiguration.setEncryption("BestEffort");
        receiverConfiguration.setListenPort("On");
        receiverConfiguration.setDefaultCall_Protocol("SIP");
        receiverConfiguration.setDefaultTransport("TLS");
        receiverConfiguration.setIce_DefaultCandidate("Host");
        receiverConfiguration.setMTU(1500);
        receiverConfiguration.setDefaultCall_Rate(60000);
        receiverConfiguration.setMaxTransmitCallRate(60000);
        receiverConfiguration.setMaxReceiveCallRate(60000);
        return receiverConfiguration;
    }


    @Override
    public String toString() {
        return "My_Configuration [Encryption=" + Encryption
                + ", ListenPort=" + ListenPort
                + ", DefaultCall_Protocol=" + DefaultCall_Protocol
                + ", DefaultTransport=" + DefaultTransport
                + ", Ice_DefaultCandidate=" + Ice_DefaultCandidate
                + ", MTU=" + MTU
                + ", DefaultCall_Rate=" + DefaultCall_Rate
                + ", MaxTransmitCallRate=" + MaxTransmitCallRate
                + ", MaxReceiveCallRate=" + MaxReceiveCallRate
                + "]";
    }
}
