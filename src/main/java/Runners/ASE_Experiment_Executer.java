package Runners;
import Clustering.ClusteringRules;
import CiscoSimulation.CiscoSimulation;
import Mining.C45Application;
import Mining.MineRuleUsingPart;
import SBRM.CG.Decoding;
import SBRM.CPLRules.CPLRule;
import SBRM.CPLRules.ReadRules;
import SBRM.CPLRules.WriteRulesAndConfidence;
import jmetal.metaheuristics.nsgaII.NSGAII_main;
import jmetal.metaheuristics.randomSearch.RandomSearch_main;
import org.apache.commons.io.FileUtils;
import Utils.CountConfigurationsGenerated;
import Utils.MergeTwoCSVs;
import Utils.WriteTime;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ASE_Experiment_Executer {

    public static void main( String[] args ) throws Exception {
    	String outputDirectory = "ASE_AllFiles/ExperimentData/";
    	int runNumber = 1;
		int totalIterations = 5;
		String ruleMiningAlgorithm = "C45"; //{C45, PART}
		String searchAlgorithm = "NSGA-II"; //{"NSGA-II","NSGA-III", "RS"}
		refineRuleSet(outputDirectory, searchAlgorithm, ruleMiningAlgorithm, runNumber, totalIterations);

		//      in case of using a jar file
		//		refineRuleSet(outputDirectory, args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
    }
    
	
    public static void refineRuleSet(String outputDirectory, String searchAlgorithm, String ruleMiningAlgorithm, int runNumber, int totalIterations) throws Exception {
		// writing the selected rule mining algorithm for later use
    	Files.write(Paths.get("src/main/resources/SAs.text"), (searchAlgorithm + "\n" + ruleMiningAlgorithm) .getBytes());

		int requiredPopulation=500;
        int evaluations=50000;
        int population=500;
        String [] para= {Integer.toString(population),Integer.toString(evaluations)}; //    {populationCount, evaluationsCount=20000}
        	 for (int iterationIndex=1;iterationIndex<=totalIterations;iterationIndex++){

        	    // Generating ConfigurationData
		        long timeTakenToGeneateConfigurationData =0;
		        long estimatedTime = 0;
		        if (searchAlgorithm.contains("NSGA-II")){	// Search
		        	estimatedTime = NSGAII_main.main(para);
		        }
		        if (searchAlgorithm.contains("RS")){	// Random
			        estimatedTime = RandomSearch_main.main(para);
		        }
		        timeTakenToGeneateConfigurationData+=estimatedTime;
		        int count= CountConfigurationsGenerated.countConfigurations("VAR");
		        population=requiredPopulation-count;
		        System.out.println("Count:-"+count);
		        while(population>0){
		            para[1]= Integer.toString(population);
			        if (searchAlgorithm.contains("NSGA-II")){	// Search
			        	estimatedTime = NSGAII_main.main(para);
			        }
			        if (searchAlgorithm.contains("RS")){	// Random
				        estimatedTime = RandomSearch_main.main(para);
			        }
		            timeTakenToGeneateConfigurationData+=estimatedTime;
		            count= CountConfigurationsGenerated.countConfigurations("VAR");
		            System.out.println("Count:-"+count);
		            population=requiredPopulation-count;
		        }
		        
		       //Removing extra configurations if there are any
		        CountConfigurationsGenerated.removeExtraConfigurations("VAR",requiredPopulation);
		        CountConfigurationsGenerated.removeExtraConfigurations("FUN",requiredPopulation);
		        
		        //Write the time used for generating the data
		        String timeTaken="Time Taken For "+searchAlgorithm+"-"+ruleMiningAlgorithm+" R"+(runNumber)+"."+(iterationIndex)+":-	"+timeTakenToGeneateConfigurationData;
				System.out.println(timeTaken);
				WriteTime.write(outputDirectory + "Time.CSV", timeTaken);
		        
		        //for converting integer variables to categorical
		    	Decoding.ReadData("VAR" ,"VAR.csv") ;


/* ************For demonstration purposes we simulate the execution part by assigning a random system state to each configuration instead of running the VCS systems to make the call******** */
		    	String sourceFilePath ="VAR.csv";
		    	CiscoSimulation.simulate("VAR.csv", "ConfigurationStatus.csv");

				//Merging new configurations to existing dataset
		    	String sourceFileToMergetPath=	"ConfigurationStatus.csv";
	        	String destinationFileToMergetPath=	 outputDirectory + "ConfigurationsAndStatuses-"+searchAlgorithm+"-"+ruleMiningAlgorithm+"-RUN"+runNumber+".csv";

	        	if (iterationIndex==1){
					MergeTwoCSVs.merge("ASE_AllFiles/ExperimentData/ZeroIteration/ConfigurationStatus.csv",destinationFileToMergetPath);
				}

	        	MergeTwoCSVs.merge(sourceFileToMergetPath,destinationFileToMergetPath);
		    	Thread.sleep(1000);

		    	//Mine the rules
		    	String constraintsOutputFilePath="Constraints.txt";
		    	String treeDestinationFilePath="C45Result.txt";
	        	if (ruleMiningAlgorithm.contains("PART")){	 // Using PART
					MineRuleUsingPart.mineRules( destinationFileToMergetPath, constraintsOutputFilePath);
	        	}
	        	if (ruleMiningAlgorithm.contains("C45")){	// Using C45
	          	C45Application.run(destinationFileToMergetPath,treeDestinationFilePath, constraintsOutputFilePath);
	        	}
		    	Thread.sleep(5000);

		    	// writing the rules into excel to use them for generating configuration data
		        CPLRule[] constraints= ReadRules.getRulesFromFile(constraintsOutputFilePath);
//		        FileUtils.forceDelete(new File(baseFolderPath+"constraints.xlsx"));
		        
		        // cluster the rules
		        constraints = ClusteringRules.cluster(constraints) ;
		        String sheetName= "R"+runNumber+"."+iterationIndex;
		        Thread.sleep(5000);
		        WriteRulesAndConfidence.WriteRulesToExcel("constraints-Search-PART.xlsx", constraints, sheetName) ;
		        Thread.sleep(5000);
		        
		    	//Moving files to relevant folders
				File source = new File("VAR" );
				File dest = new File(outputDirectory + "RUN "+(runNumber)+"/"+"Iteration "+(iterationIndex)+"/"+searchAlgorithm+"/"+ruleMiningAlgorithm+"/");
		    	FileUtils.moveFileToDirectory(source, dest, true);
		    	source = new File("FUN");
		    	FileUtils.moveFileToDirectory(source, dest, true);
		    	
		    	//Moving files to relevant folders final final configurations+statuses file 
				source = new File("VAR.csv");
		    	FileUtils.moveFileToDirectory(source, dest, true);

		    	source = new File("Constraints.txt");
		    	FileUtils.moveFileToDirectory(source, dest, true);
		    	source = new File("ConfigurationStatus.csv");
		    	FileUtils.moveFileToDirectory(source, dest, true);

		    	if (ruleMiningAlgorithm.contains("C45")){	// moving C45Result.txt file
		    		source = new File("C45Result.txt");
			    	FileUtils.moveFileToDirectory(source, dest, true);
		        	}

        	 }// end of iterations
        	 	File source = new File("constraints-Search-PART.xlsx");
        	 	File dest = new File(outputDirectory + "RUN "+(runNumber)+"/"+searchAlgorithm+"-"+ruleMiningAlgorithm+"-constraints.xlsx");
		    	FileUtils.moveFile(source, dest);

				//cleaning
				File dir = new File("./");
				for (File file : dir.listFiles()) {
					System.out.println(file.getName());
					if (file.getName().contains(".log") || file.getName().contains(".arff")) {
						file.delete();
					}
				}

        }// end of runs loops

}
