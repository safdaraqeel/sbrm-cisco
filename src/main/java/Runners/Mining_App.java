package Runners;

import Clustering.ClusteringRules;
import Mining.C45Application;
import Mining.MineRuleUsingPart;
import SBRM.CPLRules.CPLRule;
import SBRM.CPLRules.ReadRules;
import SBRM.CPLRules.WriteRulesAndConfidence;


public class Mining_App {

    public static void main( String[] args ) throws Exception {
        String baseFolderPath = "MiningTempStuff/";
        String configurationDataPath = baseFolderPath + "ConfigurationsDataWithSystemStates.csv";

        //Output Files for C4.5 and PART
        String partConstraintsPath = baseFolderPath+"Part_Constraints.txt";
        String c45ConstraintsPath = baseFolderPath+"C45_Constraints.txt";
        String treeDestinationFilePath = baseFolderPath+"C45_Result.txt";

        //mining rules with both algorithms (C4.5 and PART)
        MineRuleUsingPart.mineRules( configurationDataPath, partConstraintsPath);
        C45Application.run(configurationDataPath,treeDestinationFilePath, c45ConstraintsPath);
        Thread.sleep(5000);

        // writing the rules into excel file and clustering
        CPLRule[] part_Constraints= ReadRules.getRulesFromFile(partConstraintsPath);
        part_Constraints = ClusteringRules.cluster(part_Constraints) ;
        WriteRulesAndConfidence.WriteRulesToExcel(baseFolderPath+"Part_Constraints.xlsx", part_Constraints, "clusteredRules") ;
        CPLRule[] c45_Constraints= ReadRules.getRulesFromFile(partConstraintsPath);
        c45_Constraints = ClusteringRules.cluster(c45_Constraints) ;
        WriteRulesAndConfidence.WriteRulesToExcel(baseFolderPath+"Part_Constraints.xlsx", c45_Constraints, "clusteredRules") ;


    }


}



