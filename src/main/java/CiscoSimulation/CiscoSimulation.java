package CiscoSimulation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

public class CiscoSimulation {

    public static void simulate(String sourcePpath, String destinationPath) throws IOException {
        System.out.println("Simulating the execution of conference call");
        String data = "";
        List<String> allLines = Files.readAllLines(Paths.get(sourcePpath));
        for (String line : allLines) {
            if (line.contains("NotDone")){
                data += line.replace("NotDone",getRandomState()) + "\n";
            }else {
                data += line + "\n";
            }
        }
        System.out.println(data);
        Files.write(Paths.get(destinationPath), data.getBytes());
    }

    public static String getRandomState() {
        Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
        int c= rand.nextInt(4);
        if (c==0){
            return "ConnectedFailed";
        }else if (c==1){
            return "ConnectedConnected";
        }else if (c==2){
            return "FailedConnected";
        }else if (c==3){
            return "FailedFailed";
        }
        return "Invalid";
    }

    public static void main( String[] args ) throws Exception {
        simulate("src/main/resources/InitialRandom.csv", "src/main/resources/ConfigurationStatus.csv");
    }

}
