package Clustering;

import SBRM.CPLRules.CPLRule;
import SBRM.CPLRules.ReadRules;
import weka.clusterers.SimpleKMeans;
import weka.core.DistanceFunction;
import weka.core.EuclideanDistance;
import weka.core.Instances;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;

public class ClusteringRules {

	public static void main(String[] args) throws Exception {

		String rulesOutputFilePath = "ASE_AllFiles/Constraints.txt";
		CPLRule[] rules = ReadRules.getRulesFromFile(rulesOutputFilePath);
		for (int i = 0; i < rules.length; i++) {
			 System.err.println(rules[i].getExpression()+" "+rules[i].getViolate()+" "+rules[i].getSupport()+rules[i].getConfidence()+rules[i].getCluster()+"\n");
		}

		rules = cluster(rules);

		for (int i = 0; i < rules.length; i++) {
			 System.out.println(rules[i].getExpression()+" "+
			 rules[i].getViolate()+" "+rules[i].getSupport()+
			 +rules[i].getConfidence()+rules[i].getCluster()+"\n");
		}

	}

	public static CPLRule[] cluster(CPLRule[] rules) throws Exception {
		try {
			// getting maximum values
			double maxVilolation=0;
			double maxSupport=0;
			for (int i = 0; i < rules.length; i++) {
				if (rules[i].getViolate()>maxVilolation){
					maxVilolation=rules[i].getViolate();
				}if (rules[i].getSupport()>maxSupport){
					maxSupport=rules[i].getSupport();
				}
			}

			String headerString = " @relation constraints \n @attribute violation numeric \n @attribute support numeric \n @attribute confidence numeric\n @data \n\n ";
			String dataString = "";// "1,2,1 \n 2.0,3.3,0.9\n0.2,0.3,0.9\n";

			for (int i = 0; i < rules.length; i++) {
				dataString += rules[i].getViolate()/(maxSupport+maxVilolation) + "," + rules[i].getSupport()/(maxSupport+maxVilolation) + ","
				+ rules[i].getConfidence() + "\n";
			}
			
//			System.out.println(dataString);
			SimpleKMeans kmeans = new SimpleKMeans();
			kmeans.getDistanceFunction();
			DistanceFunction df = new EuclideanDistance();;
			kmeans.setDistanceFunction(df );
			kmeans.setSeed(10);
			kmeans.setNumClusters(3);
			kmeans.setPreserveInstancesOrder(true);

			InputStream is = new ByteArrayInputStream((headerString + dataString).getBytes());
			BufferedReader datafile = new BufferedReader(new InputStreamReader(is));
			Instances data = new Instances(datafile);
			kmeans.buildClusterer(data);

			System.out.println(kmeans.toString());
			Instances centroids= kmeans.getClusterCentroids();
			
			double[] cluster1 = centroids.get(0).toDoubleArray();
			double[] cluster2 = centroids.get(1).toDoubleArray();
			double[] cluster3 = centroids.get(2).toDoubleArray();
			System.out.println("Centroids are:-	"+centroids);
			
			double [] rankingValue={ (cluster1[1]+cluster1[2]-cluster1[0]),(cluster2[1]+cluster2[2]-cluster2[0]),(cluster3[1]+cluster3[2]-cluster3[0])};
			Hashtable< Double, Integer> rank= new Hashtable<Double, Integer>();
			rank.put(rankingValue[0], 0);
			rank.put(rankingValue[1], 1);
			rank.put(rankingValue[2], 2);
			
			double min=4,  max=0;
			for (int i = 0; i < rankingValue.length; i++) {
				if (rankingValue[i]>max){
					max=rankingValue[i];
				}
				if (rankingValue[i]<min){
					min=rankingValue[i];
				}				
			}
			
			int maxIndex=rank.get(max);
			int minIndex=rank.get(min);
			rank.remove(max);
			rank.remove(min);
			int mediumIndex=rank.values().iterator().next();
			
			int[] assignments = kmeans.getAssignments();
			for (int i = 0; i < assignments.length; i++) {
				if (assignments[i]==maxIndex){
					rules[i].setCluster("High");
				}if (assignments[i]==minIndex){
					rules[i].setCluster("Low");
				}if (assignments[i]==mediumIndex){
					rules[i].setCluster("Medium");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rules;

	}

}
