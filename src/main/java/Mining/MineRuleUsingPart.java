package Mining;

import weka.classifiers.rules.PART;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;



/**
 * Hello world!
 *
 */
public class MineRuleUsingPart {

	
	public static void mineRules(String sourceFilePath,String destinationFilePath){
		try {
			DataManipulation.converCSV2ARFF(sourceFilePath,"Data.arff");
			PART obj = new PART();
			DataSource source = new DataSource("Data.arff");
			 Instances data = source.getDataSet();
			 System.out.println("Total Attributes are:"+data.numAttributes());

			 // saving after removing certain attributes
			 ArffSaver saver = new ArffSaver();
             saver.setInstances(data);
             saver.setFile(new File("AllData.arff"));
             saver.writeBatch();
             
			 // setting class attribute if the data format does not provide this information
			 // For example, the XRFF format saves the class attribute information as well
			 if (data.classIndex() == -1)
			   data.setClassIndex(data.numAttributes() - 1);
	        	System.out.println(data.toSummaryString());
	        	//System.out.println(data);
	        	
	        	// writing the rules to outputfile
	        	System.setOut(new PrintStream(new FileOutputStream(destinationFilePath)));
	        	System.out.println("This is test output");
	        	 
	        	System.out.println("done");	    
	        	String[] argv = {"-t","AllData.arff","-i","-x","10","-c","last"};
	            PART.main(argv);
	            System.setOut(System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.out.println("Hello Weka!");
		mineRules("ConfigurationStatus.csv","Data.arff");
	}
}
