package Mining;

import java.io.*;
import java.util.Scanner;

public class DataManipulation {

	public static void converCSV2ARFF(String sourceFilePath,String destinationFilePath){
		String header ="@relation Cisco\n\n\n"+
				"@attribute Encryption {On,Off,BestEffort}\n"+
				"@attribute Encryption_s2 {On,Off,BestEffort}\n"+
				"@attribute Encryption_s3 {On,Off,BestEffort}\n"+
				"@attribute ListenPort {On,Off}\n"+
				"@attribute ListenPort_s2 {On,Off}\n"+
				"@attribute ListenPort_s3 {On,Off}\n"+
				"@attribute DefaultCall_Protocol  {Auto, H323,H320,Sip}\n"+
				"@attribute DefaultCall_Protocol_s2  {Auto, H323,H320,Sip}\n"+
				"@attribute DefaultCall_Protocol_s3  {Auto, H323,H320,Sip}\n"+
				"@attribute DefaultTransport {Auto,Tls,TCP,UDP}\n"+
				"@attribute DefaultTransport_s2 {Auto,Tls,TCP,UDP}\n"+
				"@attribute DefaultTransport_s3 {Auto,Tls,TCP,UDP}\n"+
				"@attribute Ice_DefaultCandidate {Host,Relay,Rflx}\n"+
				"@attribute Ice_DefaultCandidate_s2 {Host,Relay,Rflx}\n"+
				"@attribute Ice_DefaultCandidate_s3  {Host,Relay,Rflx}\n"+
				"@attribute MTU NUMERIC\n"+
				"@attribute MTU_s2 NUMERIC\n"+
				"@attribute MTU_s3 NUMERIC\n"+
				"@attribute DefaultCall_Rate NUMERIC\n"+
				"@attribute DefaultCall_Rate_s2 NUMERIC\n"+
				"@attribute DefaultCall_Rate_s3 NUMERIC\n"+
				"@attribute MaxTransmitCallRate NUMERIC\n"+
				"@attribute MaxTransmitCallRate_s2 NUMERIC\n"+
				"@attribute MaxTransmitCallRate_s3 NUMERIC\n"+
				"@attribute MaxReceiveCallRate NUMERIC\n"+
				"@attribute MaxReceiveCallRate_s2 NUMERIC\n"+
				"@attribute MaxReceiveCallRate_s3 NUMERIC\n"+
				"@attribute ConferenceStatus_Status {ConnectedConnected,ConnectedFailed,FailedConnected,FailedFailed}\n"+
				"@data\n\n\n";
		String data="";

		File sourceFile = new File(sourceFilePath);
		File destinationFile = new File(destinationFilePath);
		try {
			Scanner scan = new Scanner(sourceFile);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (!line.toLowerCase().contains("encryption")){
					line=line.replaceAll(";", ",")+"\n";
					data+=line;
				}
			}
			data=data.replaceAll("DisconnectingFailed", "ConnectedFailed");
			data=data.replaceAll("Ringing", "Failed");
			data=data.replaceAll("Idle", "Failed");
			data=data.replaceAll("DisconnectingConnected", "ConnectedConnected");
			data=data.replaceAll("RingingFailed", "FailedFailed");
			data=data.replaceAll("RingingConnected", "FailedConnected");
			//data=data.replaceAll("Encryption; Encryption_s2; Encryption_s3;ListenPort;ListenPort_s2;ListenPort_s3;DefaultCall_Protocol;DefaultCall_Protocol_s2;DefaultCall_Protocol_s3;DefaultTransport;DefaultTransport_s2;DefaultTransport_s3;Ice_DefaultCandidate;Ice_DefaultCandidate_s2;Ice_DefaultCandidate_S3;MTU;MTU_s2;MTU_s3;DefaultCall_Rate;DefaultCall_Rate_s2;DefaultCall_Rate_s3;MaxTransmitCallRate;MaxTransmitCallRate_s2;MaxTransmitCallRate_s3;MaxReceiveCallRate;MaxReceiveCallRate_s2;MaxReceiveCallRate_s3;Done","");
			// writing again
			FileWriter fw = new FileWriter(destinationFile,false);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(header+data);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	public static void main(String[] args) {
		converCSV2ARFF("AllData.csv","AllData.arff");
	}
}
