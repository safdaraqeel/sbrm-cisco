package Mining;

import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;


public class C45Application {

	public static void mineRules(String sourceFilePath,String destinationFilePath){
		try {
			 DataManipulation.converCSV2ARFF(sourceFilePath,"Data.arff");
			 DataSource source = new DataSource("Data.arff");
			 Instances data = source.getDataSet();
//			 System.out.println(data);
			 System.out.println("Total Attributes are:"+data.numAttributes());

			 // saving after removing certain attributes
			 ArffSaver saver = new ArffSaver();
             saver.setInstances(data);
             saver.setFile(new File("AllData.arff"));
             saver.writeBatch();

			 // setting class attribute if the data format does not provide this information
			 // For example, the XRFF format saves the class attribute information as well
			 if (data.classIndex() == -1)
			   data.setClassIndex(data.numAttributes() - 1);
	        	System.out.println(data.toSummaryString());
	        	//System.out.println(data);
	        	
	        	// writing the rules to outputfile
	        	System.setOut(new PrintStream(new FileOutputStream(destinationFilePath)));

	        	//Make tree
	            J48 tree = new J48();
//	            String p= "-t AllData.arff -c last -x 10 -s 1 -W weka.classifiers.trees.J48 -C 0.25";
//	   		 	String [] argv= p.split(" ");
	            String[] argv = {"-t","AllData.arff","-i","-x","10","-c","last"};
	            tree.setOptions(argv);
	            tree.buildClassifier(data);
	            tree.main(argv);
	            System.out.println(tree.graph().toString());
	            System.setOut(System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	 public static void main(String[] args) throws Exception {
		System.out.println("Hello Weka!");
		mineRules("AllData.csv","AllData.arff");
		String tree = new String(Files.readAllBytes(Paths.get("tree.txt")), StandardCharsets.UTF_8);
		 //System.out.println(tree);
		 String [] array=tree.split("\n");
		 List<String> textualTreeNodes = Arrays.asList(array);
		 ExtractRuleFromDescisionTree.extractRules("tree.txt","J48Rules.txt");
	}
	 
	 public static void run(String sourceFilePath,String destinationFilePath,String rulesDestinationFilePath) throws Exception {
			System.out.println("Hello Weka!");
			mineRules(sourceFilePath,destinationFilePath);			
			String tree = new String(Files.readAllBytes(Paths.get(destinationFilePath)), StandardCharsets.UTF_8);
			 ExtractRuleFromDescisionTree.extractRules(destinationFilePath,rulesDestinationFilePath);
		}
}
