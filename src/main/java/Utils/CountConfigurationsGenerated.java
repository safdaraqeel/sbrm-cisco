package Utils;

import java.io.*;
import java.util.Scanner;

public class CountConfigurationsGenerated {

	public static int countConfigurations(String filePath){
			int totalConfiguraitons=0;
//			filePath = "VAR";
			String text = "";
			File fileName = new File(filePath);
			try {
				Scanner scan = new Scanner(fileName);
				while (scan.hasNextLine()) {
					String line = scan.nextLine();
					totalConfiguraitons++;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		
		
		return totalConfiguraitons;
	}
	
	public static void removeExtraConfigurations(String filePath, int requiredPopulation){
		int totalConfiguraitons=0;
//		filePath = "VAR";
		String configurations = "";
		
		File fileName = new File(filePath);
		try {
			Scanner scan = new Scanner(fileName);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				totalConfiguraitons++;
				if (totalConfiguraitons<=requiredPopulation){
					configurations+=line+"\n";
				}	
			}
			
	    	// writing again
	    	FileWriter fw = new FileWriter(fileName,false);
	    	BufferedWriter bw = new BufferedWriter(fw);
	    	bw.write(configurations);
	    	bw.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
}
	
}
