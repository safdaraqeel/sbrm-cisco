package Utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MergeTwoCSVs {

	public static void merge(String sourceFilePath, String destinationFilePath) {
		try {
			String textOfSource = new String(Files.readAllBytes(Paths.get(sourceFilePath)), StandardCharsets.UTF_8);
			textOfSource=textOfSource.replaceAll("Encryption; Encryption_s2; Encryption_s3;ListenPort;ListenPort_s2;ListenPort_s3;DefaultCall_Protocol;DefaultCall_Protocol_s2;DefaultCall_Protocol_s3;DefaultTransport;DefaultTransport_s2;DefaultTransport_s3;Ice_DefaultCandidate;Ice_DefaultCandidate_s2;Ice_DefaultCandidate_S3;MTU;MTU_s2;MTU_s3;DefaultCall_Rate;DefaultCall_Rate_s2;DefaultCall_Rate_s3;MaxTransmitCallRate;MaxTransmitCallRate_s2;MaxTransmitCallRate_s3;MaxReceiveCallRate;MaxReceiveCallRate_s2;MaxReceiveCallRate_s3;Done;Identifier", "");
			FileWriter fw = new FileWriter(destinationFilePath, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
			out.println("\n"+textOfSource);
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		merge("AllFiles/InitialConfigurationsAndStatuses.csv", "AllFiles/ConfigurationsAndStatusesRun1.csv");
	}

}
