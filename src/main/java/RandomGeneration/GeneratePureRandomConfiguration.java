package RandomGeneration;

import Clustering.ClusteringRules;
import Mining.C45Application;
import Mining.MineRuleUsingPart;
import SBRM.CG.Decoding;
import SBRM.CG.EncodedConfigurableParameters;
import SBRM.CPLRules.CPLRule;
import SBRM.CPLRules.ReadRules;
import SBRM.CPLRules.WriteRulesAndConfidence;
import Utils.My_Writer;

public class GeneratePureRandomConfiguration {

	public static void getPureRandomConfiguration(String path, int totalConfigurations) {
		//int totalConfigurations = 100;
		EncodedConfigurableParameters[] configurations = new EncodedConfigurableParameters[totalConfigurations];
		String ConfigurationData = "";
		for (int i = 0; i < configurations.length; i++) {
			configurations[i] = new EncodedConfigurableParameters();
			configurations[i].getRandomConfiguration();

			ConfigurationData += configurations[i].getEncryption() + " ";
			ConfigurationData += configurations[i].getEncryption_s2() + " ";
			ConfigurationData += configurations[i].getEncryption_s3() + " ";
			ConfigurationData += configurations[i].getListenPort() + " ";
			ConfigurationData += configurations[i].getListenPort_s2() + " ";
			ConfigurationData += configurations[i].getListenPort_s3() + " ";
			ConfigurationData += configurations[i].getDefaultCall_Protocol() + " ";
			ConfigurationData += configurations[i].getDefaultCall_Protocol_s2() + " ";
			ConfigurationData += configurations[i].getDefaultCall_Protocol_s3() + " ";
			ConfigurationData += configurations[i].getDefaultTransport() + " ";
			ConfigurationData += configurations[i].getDefaultTransport_s2() + " ";
			ConfigurationData += configurations[i].getDefaultTransport_s3() + " ";
			ConfigurationData += configurations[i].getIce_DefaultCandidate() + " ";
			ConfigurationData += configurations[i].getIce_DefaultCandidate_s2() + " ";
			ConfigurationData += configurations[i].getIce_DefaultCandidate_s3() + " ";
			ConfigurationData += configurations[i].getMTU() + " ";
			ConfigurationData += configurations[i].getMTU_s2() + " ";
			ConfigurationData += configurations[i].getMTU_s3() + " ";
			ConfigurationData += configurations[i].getDefaultCall_Rate() + " ";
			ConfigurationData += configurations[i].getDefaultCall_Rate_s2() + " ";
			ConfigurationData += configurations[i].getDefaultCall_Rate_s3() + " ";
			ConfigurationData += configurations[i].getMaxTransmitCallRate() + " ";
			ConfigurationData += configurations[i].getMaxTransmitCallRate_s2() + " ";
			ConfigurationData += configurations[i].getMaxTransmitCallRate_s3() + " ";
			ConfigurationData += configurations[i].getMaxReceiveCallRate() + " ";
			ConfigurationData += configurations[i].getMaxReceiveCallRate_s2() + " ";
			ConfigurationData += configurations[i].getMaxReceiveCallRate_s3() + "\n";
		}

		// String header="Encryption;Encryption_s2;Encryption_s3;ListenPort;ListenPort_s2;ListenPort_s3;DefaultCall_Protocol;DefaultCall_Protocol_s2;DefaultCall_Protocol_s3;DefaultTransport;DefaultTransport_s2;DefaultTransport_s3;Ice_DefaultCandidate;Ice_DefaultCandidate_s2;Ice_DefaultCandidate_s3;MTU;MTU_s2;MTU_s3;DefaultCall_Rate;DefaultCall_Rate_s2;DefaultCall_Rate_s3;MaxTransmitCallRate;MaxTransmitCallRate_s2;MaxTransmitCallRate_s3;MaxReceiveCallRate;MaxReceiveCallRate_s2;MaxReceiveCallRate_s3\n";
		My_Writer.write(path, ConfigurationData);
	}

	public static void main( String[] args ) throws Exception {
		getPureRandomConfiguration("src/main/resources/InitialRandom", 10);
		Decoding.ReadData("src/main/resources/InitialRandom", "src/main/resources/InitialRandom.csv");
	}


}
